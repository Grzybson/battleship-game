package com.battleship.game;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@EnableWebMvc
public class BattleShipApplication {

    public static void main(String[] args) {
        SpringApplication.run(BattleShipApplication.class, args);
    }

    public static String getMyURL() {
        return "http://localhost:8080" ;
    }
    public static final String API_PREFIX = "/battleShip";

}
