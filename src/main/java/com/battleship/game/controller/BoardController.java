package com.battleship.game.controller;

import com.battleship.game.BattleShipApplication;
import com.battleship.game.dto.board.BoardDto;
import com.battleship.game.dto.board.CreateBoardDto;
import com.battleship.game.dto.board.UpdateBoardDto;
import com.battleship.game.service.BoardService;
import com.battleship.game.service.exceptions.board.BadBoardSize;
import com.battleship.game.service.exceptions.board.BoardNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BattleShipApplication.API_PREFIX + "/board")
public class BoardController {

    @Autowired
    private BoardService boardService;


    @GetMapping
    public List<BoardDto> getBoards() {
        return boardService.getBoards();
    }

    @GetMapping("/{boardId}")
    public BoardDto getBoardById(@PathVariable String boardId) throws BoardNotFound {
        return boardService.getBoardById(boardId);

    }

    @PostMapping
    public BoardDto addBoard(@RequestBody CreateBoardDto createBoardDto) throws BadBoardSize {
        return boardService.addBoard(createBoardDto);
    }

    @PutMapping("/{boardId}")
    public BoardDto updateBoard(@PathVariable String boardId, @RequestBody UpdateBoardDto boardDto)
            throws BoardNotFound {
        return boardService.updateBoard(boardId, boardDto);

    }

    @DeleteMapping("/{boardId}")
    public BoardDto deleteBoardById(@PathVariable String boardId) throws BoardNotFound {
        return boardService.deleteBoardById(boardId);
    }


}
