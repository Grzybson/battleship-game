package com.battleship.game.controller;


import com.battleship.game.BattleShipApplication;
import com.battleship.game.dto.game.CreateUpdateGameDto;
import com.battleship.game.dto.game.GameDto;
import com.battleship.game.service.GameService;
import com.battleship.game.service.exceptions.game.GameNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BattleShipApplication.API_PREFIX + "/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @GetMapping
    public List<GameDto> getAllGames() {
        return gameService.getAllGames();
    }

    @GetMapping("/{gameId}")
    public GameDto getGameById(@PathVariable String gameId) throws GameNotFound {
        return gameService.getGameById(gameId);
    }

    @PostMapping
    public GameDto createGame(@RequestBody CreateUpdateGameDto gameDto) throws GameNotFound {
        return gameService.createGame(gameDto);
    }

    @PutMapping("/{gameId}")
    public GameDto updateGame(@RequestBody CreateUpdateGameDto gameDto, @PathVariable String gameId) throws GameNotFound {
        return gameService.updateGame(gameDto, gameId);
    }

    @DeleteMapping("/{gameId}")
    public GameDto deleteGameById(@PathVariable String gameId) throws GameNotFound {
        return gameService.deleteGame(gameId);
    }



}
