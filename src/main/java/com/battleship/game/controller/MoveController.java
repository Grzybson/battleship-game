package com.battleship.game.controller;

import com.battleship.game.BattleShipApplication;
import com.battleship.game.dto.move.CreateMoveDto;
import com.battleship.game.dto.move.MoveDto;
import com.battleship.game.dto.move.UpdateMoveDto;
import com.battleship.game.service.MoveService;
import com.battleship.game.service.exceptions.move.MoveAlreadyDone;
import com.battleship.game.service.exceptions.move.MoveNotFound;
import com.battleship.game.service.exceptions.move.MoveOutsideBoard;
import com.battleship.game.service.exceptions.move.SecondMoveInTheSameRound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.net.URI;
import java.util.List;
import java.util.UUID;

import static com.battleship.game.BattleShipApplication.API_PREFIX;
import static com.battleship.game.BattleShipApplication.getMyURL;
import static com.battleship.game.Utils.toJsonString;

@RestController
@RequestMapping(BattleShipApplication.API_PREFIX + "/move")
public class MoveController {

    @Autowired
    private MoveService moveService;

    @GetMapping
    public List<MoveDto> getAllMoves() {
        return moveService.getAllMoves();
    }

    @GetMapping("/{moveId}")
    public MoveDto getMoveById(@PathVariable UUID moveId) throws MoveNotFound {
        return moveService.getMoveById(moveId);
    }

    @PostMapping
    public MoveDto addMove(@RequestBody CreateMoveDto moveDto)
            throws MoveAlreadyDone, SecondMoveInTheSameRound, MoveOutsideBoard {

        return moveService.addMove(moveDto);
    }


    @PutMapping("/{moveId}")
    public MoveDto updateMove(@PathVariable UUID moveId, @RequestBody UpdateMoveDto moveDto)
            throws MoveAlreadyDone, MoveNotFound, MoveOutsideBoard {

        return moveService.updateMovePosition(moveId, moveDto);
    }

    @DeleteMapping("/{moveId}")
    public MoveDto deleteMoveById(@PathVariable UUID moveId) throws MoveNotFound {
        return moveService.deleteMoveById(moveId);
    }
}
