package com.battleship.game.controller;

import com.battleship.game.BattleShipApplication;
import com.battleship.game.dto.ship.CreateUpdateShipDto;
import com.battleship.game.dto.ship.ShipDto;
import com.battleship.game.service.ShipService;
import com.battleship.game.service.exceptions.ship.ShipInvalidData;
import com.battleship.game.service.exceptions.ship.ShipNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BattleShipApplication.API_PREFIX + "/ship")
public class ShipController {

    @Autowired
    private ShipService shipService;

    @GetMapping
    public List<ShipDto> getAllShips() {
        return shipService.getAllShips();
    }

    @GetMapping("/{shipId}")
    public ShipDto getShipById(@PathVariable String shipId) throws ShipNotFound {
        return shipService.getShipById(shipId);
    }

    @PostMapping
    public ShipDto addShip(@RequestBody CreateUpdateShipDto shipDto) throws ShipInvalidData {
        return shipService.placeShipOnBoard(shipDto);
    }

    @PutMapping("/{shipId}")
    public ShipDto updateShip(@RequestBody CreateUpdateShipDto updateShip, @PathVariable String shipId)
            throws ShipNotFound {
        return shipService.updateShipOnBoard(updateShip, shipId);
    }

    @DeleteMapping("/{shipId}")
    public ShipDto deleteShipById(@PathVariable String shipId) throws ShipNotFound {
        return shipService.deleteShipById(shipId);
    }

}
