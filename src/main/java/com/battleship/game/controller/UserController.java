package com.battleship.game.controller;


import com.battleship.game.BattleShipApplication;
import com.battleship.game.dto.user.CreateUpdateUserDto;
import com.battleship.game.dto.user.UserDto;
import com.battleship.game.service.UserService;
import com.battleship.game.service.exceptions.user.ConflictUser;
import com.battleship.game.service.exceptions.user.UserNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BattleShipApplication.API_PREFIX + "/user")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{userId}")
    public UserDto getUserById(@PathVariable String userId) throws UserNotFound {
        return userService.getUserById(userId);
    }

    @PostMapping
    public UserDto addUser(@RequestBody CreateUpdateUserDto user) throws ConflictUser {
        return userService.addUser(user);
    }

    @PutMapping("/{userId}")
    public UserDto updateUser(@RequestBody CreateUpdateUserDto user, @PathVariable String userId)
            throws UserNotFound {
        return userService.updateUser(user, userId);
    }

    @DeleteMapping("/{userId}")
    public UserDto deleteUserById(@PathVariable String userId) throws UserNotFound {
        return userService.deleteUserById(userId);
    }


}
