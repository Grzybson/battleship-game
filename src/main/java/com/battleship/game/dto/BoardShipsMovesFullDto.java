package com.battleship.game.dto;

import com.battleship.game.dto.board.BoardDto;
import com.battleship.game.dto.move.MoveDto;
import com.battleship.game.dto.ship.ShipDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BoardShipsMovesFullDto {

    public static final String SHIP = "*";

    private String id;
    private int xsize;
    private int ysize;

    private List<List<String>> shipsWithResultsView;


    private List<ShipDto> ships;
    private BoardDto board;
    private List<MoveDto> moves;


    public boolean areAllShipsSunk() {
        for (int y = 0; y < ysize; y++) {
            for (int x = 0; x < xsize; x++) {
                String resultAtPositionXandY = shipsWithResultsView.get(x).get(y);
                boolean isShipToSinkRemainsOnBoard = SHIP.equals(resultAtPositionXandY);
                if (isShipToSinkRemainsOnBoard) {
                    return false;
                }
            }
        }
        return true;
    }

}
