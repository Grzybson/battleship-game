package com.battleship.game.dto.game;

import com.battleship.game.model.GameState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateUpdateGameDto {

    private String player1Id;
    private String player2Id;
    private String board1Id;
    private String board2Id;
    private String name;
    private int round;
    private GameState status;
}
