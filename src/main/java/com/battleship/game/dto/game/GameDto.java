package com.battleship.game.dto.game;

import com.battleship.game.model.GameState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameDto {

    private String id;
    private String name;
    private int round;
    private String player1Id;
    private String player2Id;
    private String board1Id;
    private String board2Id;
    private GameState status;
}
