package com.battleship.game.dto.game;

import com.battleship.game.model.GameState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GameFullDto {
    private String id;
    private String name;
    private int round;
    private String player1Id;
    private String player1Login;
    private String player2Id;
    private String player2Login;
    private String board1Id;
    private String board2Id;
    private GameState status;
}
