package com.battleship.game.dto.move;

import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateMoveDto {

    @NotNull
    protected UUID boardId;
    @NotNull
    protected Integer round;
    @NotNull
    protected Integer x;
    @NotNull
    protected Integer y;

//    @NotNull(message = Move.BOARD_ID_IS_MANDATORY)
//    protected UUID boardId;
//
//    @NotNull(message = Move.ROUND_IS_MANDATORY)
//    @Min(value = 0, message = Move.ROUND_STARTS_AT_0)
//    protected Integer round;
//
//    @NotNull(message = Move.X_IS_MANDATORY)
//    @Min(value = 0, message = Move.BOARD_STARTS_AT_X0)
//    protected Integer x;
//
//    @NotNull(message = Move.Y_IS_MANDATORY)
//    @Min(value = 0, message = Move.BOARD_STARTS_AT_Y0)
//    protected Integer y;
}
