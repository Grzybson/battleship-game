package com.battleship.game.dto.move;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MoveDto {

    private UUID id;
    protected UUID boardId;
    protected Integer round;
    protected Integer x;
    protected Integer y;
}
