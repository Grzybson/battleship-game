package com.battleship.game.dto.move;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UpdateMoveDto {

    @NotNull
    protected Integer x;
    @NotNull
    protected Integer y;
}
