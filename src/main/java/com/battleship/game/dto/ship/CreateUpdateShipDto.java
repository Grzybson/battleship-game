package com.battleship.game.dto.ship;

import com.battleship.game.model.Orientation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CreateUpdateShipDto {

    private String boardId;
    private int x;
    private int y;
    private int size;
    private Orientation orientation;
}
