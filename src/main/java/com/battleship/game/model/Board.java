package com.battleship.game.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Board {

    public static final int X_SIZE = 10;
    public static final int Y_SIZE = 10;

    @Id
    @Column(columnDefinition = "varchar(100)")
    private String id;
    private int xSize;
    private int ySize;
}
