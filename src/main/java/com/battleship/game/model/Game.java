package com.battleship.game.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Game {

    @Id
    @Column(columnDefinition = "varchar(36)")
    private String id;
    private String name;
    private int round;
    private String player1Id;
    private String player2Id;
    private String board1Id;
    private String board2Id;
    private GameState status;
}
