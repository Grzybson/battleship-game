package com.battleship.game.model;

public enum GameState {

    GAME_NOT_STARTED,
    GAME_IN_PROGRESS,
    PLAYER_1_WON,
    PLAYER_2_WON,
    DRAW;

    public static boolean isStarted(GameState state) {
        return GAME_IN_PROGRESS.equals(state);
    }

    public static boolean isNotStarted(GameState state) {
        return GAME_NOT_STARTED.equals(state);
    }

    public static boolean gameResult(GameState state) {
        return PLAYER_1_WON.equals(state) ||
                PLAYER_2_WON.equals(state) ||
                DRAW.equals(state);
    }

}
