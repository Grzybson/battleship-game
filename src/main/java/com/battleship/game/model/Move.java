package com.battleship.game.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import java.util.Objects;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Move {

    @Id
    @Column(columnDefinition = "binary(16)")
    private UUID id;
    protected UUID boardId;
    protected Integer round;
    protected Integer x;
    protected Integer y;

    @Override
    public boolean equals(Object obj) {
        if (obj == null)
            return false;

        if (!(obj instanceof Move))
            return false;

        Move m = (Move) obj;

        return Objects.equals(this.getBoardId(), m.getBoardId())
                && Objects.equals(this.getX(), m.getX())
                && Objects.equals(this.getY(), m.getY()); //on purpose - only position and board matters
    }
}
