package com.battleship.game.model;

public enum Orientation {
    HORIZONTAL,
    VERTICAL;
}
