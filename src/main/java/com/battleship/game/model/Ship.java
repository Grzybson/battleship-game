package com.battleship.game.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Ship {

    @Id
    @Column(columnDefinition = "varchar(100)")
    private String id;
    private String boardId;
    private int x;
    private int y;
    private int size;
    private Orientation orientation;
}
