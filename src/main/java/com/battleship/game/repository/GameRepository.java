package com.battleship.game.repository;

import com.battleship.game.model.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, String> {

    List<Game> findAllByPlayer1Id(String player1Id);
    List<Game> findAllByPlayer2Id(String player2Id);
}
