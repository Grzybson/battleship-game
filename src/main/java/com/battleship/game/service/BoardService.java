package com.battleship.game.service;

import com.battleship.game.dto.*;
import com.battleship.game.dto.board.BoardDto;
import com.battleship.game.dto.board.CreateBoardDto;
import com.battleship.game.dto.board.UpdateBoardDto;
import com.battleship.game.dto.ship.ShipDto;
import com.battleship.game.model.Board;
import com.battleship.game.repository.BoardRepository;
import com.battleship.game.service.exceptions.board.BadBoardSize;
import com.battleship.game.service.exceptions.board.BoardNotFound;
import com.battleship.game.service.mapper.BoardDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class BoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private BoardDtoMapper boardDtoMapper;

    @Autowired
    private ShipService shipService;

    @Transactional
    public List<BoardDto> getBoards() {
        return boardRepository.findAll()
                .stream()
                .map(b -> boardDtoMapper.toDto(b))
                .collect(Collectors.toList());
    }

    @Transactional
    public BoardDto getBoardById(@PathVariable String boardId) throws BoardNotFound {
        return boardRepository.findById(boardId)
                .map(b -> boardDtoMapper.toDto(b))
                .orElseThrow(() -> new BoardNotFound());
    }


    @Transactional
    public BoardDto addBoard(CreateBoardDto createBoardDto) throws BadBoardSize {
        Board board = boardDtoMapper.toModel(createBoardDto);
        if (board.getXSize() != 10 && board.getYSize() != 10) {
            throw new BadBoardSize();
        }
        Board savedBoard = boardRepository.save(board);
        return boardDtoMapper.toDto(savedBoard);

    }

    @Transactional
    public BoardDto updateBoard(@PathVariable String boardId, @RequestBody UpdateBoardDto boardDto)
            throws BoardNotFound {

        Board boardToUpdate = boardRepository.findById(boardId).orElseThrow(() -> new BoardNotFound());
        boardToUpdate.setXSize(boardDto.getXSize());
        boardToUpdate.setYSize(boardDto.getYSize());

        Board savedBoard = boardRepository.save(boardToUpdate);
        return boardDtoMapper.toDto(savedBoard);

    }

    @Transactional
    public BoardDto deleteBoardById(@PathVariable String boardId) throws BoardNotFound {
        Board boardToDelete = boardRepository.findById(boardId).orElseThrow(() -> new BoardNotFound());

        boardRepository.delete(boardToDelete);

        return boardDtoMapper.toDto(boardToDelete);
    }

    @Transactional
    public BoardShipsMovesFullDto getShipsWithMovesOnBoard(String boardId) throws BoardNotFound {
        List<ShipDto> ships = shipService.getShipsOnBoard(boardId);


        BoardDto boardDto = getBoardById(boardId);

        BoardShipsMovesFullDto boardWithShips = BoardShipsMovesFullDto.builder()
                .ships(ships)
//                .moves() ToDo
                .board(boardDto)
                .build();

        return boardWithShips;
    }

    @Transactional
    public boolean areAllShipsSunk(String boardId) throws BoardNotFound {
        BoardShipsMovesFullDto shipsAndMoves = getShipsWithMovesOnBoard(boardId);
        return shipsAndMoves.areAllShipsSunk();
    }


}
