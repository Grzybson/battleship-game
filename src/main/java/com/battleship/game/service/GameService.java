package com.battleship.game.service;

import com.battleship.game.dto.game.CreateUpdateGameDto;
import com.battleship.game.dto.game.GameDto;
import com.battleship.game.dto.game.GameFullDto;
import com.battleship.game.model.Game;
import com.battleship.game.repository.GameRepository;
import com.battleship.game.service.exceptions.game.GameNotFound;
import com.battleship.game.service.exceptions.user.UserNotFound;
import com.battleship.game.service.mapper.GameDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class GameService {


    @Autowired
    private GameDtoMapper gameDtoMapper;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private UserService userService;


    @Transactional
    public List<GameDto> getAllGames() {
        return gameRepository.findAll()
                .stream()
                .map(g -> gameDtoMapper.toDto(g))
                .collect(Collectors.toList());
    }

    @Transactional
    public GameDto getGameById(String gameId) throws GameNotFound {
        return gameRepository.findById(gameId)
                .map(g -> gameDtoMapper.toDto(g))
                .orElseThrow(GameNotFound::new);
    }

    @Transactional
    public GameDto createGame(CreateUpdateGameDto gameDto) {
        Game game = gameDtoMapper.toModel(gameDto);
        game.setId(UUID.randomUUID().toString());

        Game savedGame = gameRepository.save(game);
        return gameDtoMapper.toDto(savedGame);
    }

    @Transactional
    public GameDto updateGame(CreateUpdateGameDto gameDto, String gameId) throws GameNotFound {
        Game game = gameRepository.findById(gameId)
                .orElseThrow(GameNotFound::new);

        game.setPlayer1Id(gameDto.getPlayer1Id());
        game.setPlayer2Id(gameDto.getPlayer2Id());
        game.setName(gameDto.getName());
        game.setRound(gameDto.getRound());
        game.setStatus(gameDto.getStatus());
        game.setBoard1Id(gameDto.getBoard1Id());
        game.setBoard2Id(gameDto.getBoard2Id());

        Game savedGame = gameRepository.save(game);
        return gameDtoMapper.toDto(savedGame);
    }

    @Transactional
    public GameDto deleteGame(String gameId) throws GameNotFound {
        Game game = gameRepository.findById(gameId).orElseThrow(GameNotFound::new);

        gameRepository.delete(game);
        return gameDtoMapper.toDto(game);
    }

    @Transactional
    public List<GameFullDto> getAllPlayer1Games(String login) throws UserNotFound {
        String id = userService.mapLoginToId(login);
        return gameRepository.findAllByPlayer1Id(id)
                .stream()
                .map(g -> gameDtoMapper.toFullDto(g))
                .collect(Collectors.toList());

    }

    @Transactional
    public List<GameFullDto> getAllPlayer2Games(String login) throws UserNotFound {
        String id = userService.mapLoginToId(login);
        return gameRepository.findAllByPlayer2Id(id)
                .stream()
                .map(g -> gameDtoMapper.toFullDto(g))
                .collect(Collectors.toList());
    }


}
