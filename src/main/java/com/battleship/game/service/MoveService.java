package com.battleship.game.service;

import com.battleship.game.dto.move.CreateMoveDto;
import com.battleship.game.dto.move.MoveDto;
import com.battleship.game.dto.move.UpdateMoveDto;
import com.battleship.game.model.Board;
import com.battleship.game.model.Move;
import com.battleship.game.repository.MoveRepository;
import com.battleship.game.service.exceptions.move.MoveAlreadyDone;
import com.battleship.game.service.exceptions.move.MoveNotFound;
import com.battleship.game.service.exceptions.move.MoveOutsideBoard;
import com.battleship.game.service.exceptions.move.SecondMoveInTheSameRound;
import com.battleship.game.service.mapper.MoveDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MoveService {

    @Autowired
    MoveRepository moveRepository;

    @Autowired
    MoveDtoMapper moveDtoMapper;


    @Transactional
    public List<MoveDto> getAllMoves() {
        return moveRepository.findAll()
                .stream()
                .map(m -> moveDtoMapper.toDto(m))
                .collect(Collectors.toList());
    }

    @Transactional
    public MoveDto getMoveById(UUID moveId) throws MoveNotFound {
        return moveRepository.findById(moveId)
                .map(m -> moveDtoMapper.toDto(m))
                .orElseThrow(MoveNotFound::new);
    }


    @Transactional
    public MoveDto deleteMoveById(UUID moveId) throws MoveNotFound {
        Move move = moveRepository.findAll()
                .stream()
                .filter(m -> m.getId().equals(moveId))
                .findFirst()
                .orElseThrow(MoveNotFound::new);

        moveRepository.delete(move);
        return moveDtoMapper.toDto(move);
    }

    @Transactional
    protected boolean isMoveAlreadyDone(int x, int y, UUID boardId) {
        int movesAmount = (int) moveRepository.count();

        return movesAmount > 0;
    }

    @Transactional
    protected boolean isMoveOutsideBoard(int x, int y, UUID boardId) {
        return x < 0 || x > Board.X_SIZE || y < 0 || y > Board.Y_SIZE;
    }

    @Transactional
    public MoveDto updateMovePosition(UUID moveId, UpdateMoveDto newPosition)
            throws MoveNotFound, MoveOutsideBoard, MoveAlreadyDone {

        Move move = moveRepository.findById(moveId)
                .orElseThrow(MoveNotFound::new);

        if (isMoveOutsideBoard(newPosition.getX(), newPosition.getY(), move.getBoardId())) {
            throw new MoveOutsideBoard();
        }

        if (isMoveAlreadyDone(newPosition.getX(), newPosition.getY(), move.getBoardId())) {
            throw new MoveAlreadyDone();
        }
        move.setX(newPosition.getX());
        move.setY(newPosition.getY());

        return moveDtoMapper.toDto(move);

    }

    @Transactional
    public MoveDto addMove(CreateMoveDto moveDto) throws MoveOutsideBoard, MoveAlreadyDone, SecondMoveInTheSameRound {

        if (isMoveOutsideBoard(moveDto.getX(), moveDto.getY(), moveDto.getBoardId())) {
            throw new MoveOutsideBoard();
        }

        if (isMoveAlreadyDone(moveDto.getX(), moveDto.getY(), moveDto.getBoardId())) {
            throw new MoveAlreadyDone();
        }

//        if (isAnyMoveDoneInRound(moveDto.getBoardId(), moveDto.getRound())) {
//            throw new SecondMoveInTheSameRound();
//        }

        Move moveWithId = moveDtoMapper.toModel(moveDto);
        Move savedMove = moveRepository.save(moveWithId);
        return moveDtoMapper.toDto(savedMove);
    }




}
