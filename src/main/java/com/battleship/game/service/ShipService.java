package com.battleship.game.service;

import com.battleship.game.dto.ship.CreateUpdateShipDto;
import com.battleship.game.dto.ship.ShipDto;
import com.battleship.game.model.Ship;
import com.battleship.game.repository.ShipRepository;
import com.battleship.game.service.exceptions.board.BoardNotFound;
import com.battleship.game.service.exceptions.ship.ShipInvalidData;
import com.battleship.game.service.exceptions.ship.ShipNotFound;
import com.battleship.game.service.mapper.ShipDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ShipService {

    @Autowired
    private ShipDtoMapper shipDtoMapper;

    @Autowired
    private ShipRepository shipRepository;

    @Autowired
    private BoardService boardService;

    @Transactional
    public List<ShipDto> getAllShips() {
        return shipRepository.findAll().stream()
                .map(s -> shipDtoMapper.toDto(s))
                .collect(Collectors.toList());
    }

    @Transactional
    public ShipDto getShipById(String shipId) throws ShipNotFound {
        return shipRepository.findById(shipId)
                .map(s -> shipDtoMapper.toDto(s))
                .orElseThrow(() -> new ShipNotFound());
    }

    @Transactional
    public List<ShipDto> getShipsOnBoard(String boardId) throws BoardNotFound {
        boardService.getBoardById(boardId);

        List<ShipDto> ships = new ArrayList<>();
        for (Ship s : shipRepository.findAll()) {
            if (s.getId().equals(boardId)) {
                ships.add(shipDtoMapper.toDto(s));
            }
        }

        return ships;
    }

    @Transactional
    public ShipDto placeShipOnBoard(CreateUpdateShipDto shipDto) throws ShipInvalidData {
        shipDtoMapper.validate(shipDto);
        Ship newShip = shipDtoMapper.toModel(shipDto);
        Ship savedShip = shipRepository.save(newShip);
        return shipDtoMapper.toDto(savedShip);
    }

    public ShipDto updateShipOnBoard(CreateUpdateShipDto updateShipDto, String shipId)
            throws ShipNotFound {
        Ship ship = shipRepository.findAll().stream()
                .filter(s -> s.getId().equals(shipId))
                .findFirst()
                .orElseThrow(() -> new ShipNotFound());

        ship.setBoardId(updateShipDto.getBoardId());
        ship.setX(updateShipDto.getX());
        ship.setY(updateShipDto.getY());
        ship.setOrientation(updateShipDto.getOrientation());
        ship.setSize(updateShipDto.getSize());

        return shipDtoMapper.toDto(ship);

    }


    public ShipDto deleteShipById(String shipId) throws ShipNotFound {
        Ship ship = shipRepository.findAll().stream()
                .filter(s -> s.getId().equals(shipId))
                .findFirst()
                .orElseThrow(() -> new ShipNotFound());

        shipRepository.delete(ship);
        return shipDtoMapper.toDto(ship);
    }

}
