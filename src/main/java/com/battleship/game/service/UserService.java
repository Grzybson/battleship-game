package com.battleship.game.service;

import com.battleship.game.dto.user.CreateUpdateUserDto;
import com.battleship.game.dto.user.UserDto;
import com.battleship.game.model.User;
import com.battleship.game.repository.UserRepository;
import com.battleship.game.service.exceptions.user.ConflictUser;
import com.battleship.game.service.exceptions.user.UserNotFound;
import com.battleship.game.service.mapper.UserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDtoMapper userDtoMapper;

    @Autowired
    private UserRepository userRepository;

    public List<User> users = new ArrayList<>();

    @Transactional
    public List<UserDto> getAllUsers() {
        List<UserDto> userDtos = new ArrayList<>();
        for (User u : users) {
            userDtos.add(userDtoMapper.toDto(u));
        }
        return userDtos;
    }

    @Transactional
    public UserDto getUserById(String userId) throws UserNotFound {
        return userRepository.findById(userId)
                .map(u -> userDtoMapper.toDto(u))
                .orElseThrow(() -> new UserNotFound());
    }

    @Transactional
    public UserDto getUserByLogin(String login) throws UserNotFound {
        return userRepository.findById(login)
                .map(l -> userDtoMapper.toDto(l))
                .orElseThrow(() -> new UserNotFound());
    }

    @Transactional
    public UserDto addUser(CreateUpdateUserDto userDto) throws ConflictUser {
        User user = userDtoMapper.toModel(userDto);

        if (users.stream()
                .filter(u -> u.getLogin().equals(userDto.getLogin()))
                .findFirst()
                .orElse(null) != null)
            throw new ConflictUser();

        users.add(user);
        return userDtoMapper.toDto(user);
    }

    public UserDto updateUser(@RequestBody CreateUpdateUserDto user, @PathVariable String userId)
            throws UserNotFound {
        User userToUpdate = users.stream()
                .filter(u -> u.getId().equals(userId))
                .findFirst().orElseThrow(() -> new UserNotFound());
        userToUpdate.setLogin(user.getLogin());
        userToUpdate.setPassword(user.getPassword());
        userToUpdate.setNickname(user.getNickname());
        return userDtoMapper.toDto(userToUpdate);
    }

    @Transactional
    public UserDto deleteUserById(String userId) throws UserNotFound {
        User userToDelete = userRepository.findById(userId).orElseThrow(() -> new UserNotFound());
        if (!userToDelete.getId().isEmpty()) {
            throw new UserNotFound();
        }
        userRepository.delete(userToDelete);
        return userDtoMapper.toDto(userToDelete);

    }


    public String mapLoginToId(String login) throws UserNotFound {

        return users.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst()
                .map(User::getId)
                .orElseThrow(UserNotFound::new);
    }

    public String mapIdToLogin(String player1Id) {

        return users.stream()
                .filter(u -> u.getId().equals(player1Id))
                .findFirst()
                .map(User::getLogin)
                .orElse(null);
    }
}
