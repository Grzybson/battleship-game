package com.battleship.game.service.exceptions.board;

import com.battleship.game.model.Board;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadBoardSize extends Exception {

    public static final String MESSAGE = "Can not create board in different size like xSize:" +
            Board.X_SIZE + " and ySize: " + Board.Y_SIZE;

    public BadBoardSize() {
        super((MESSAGE));
    }

}
