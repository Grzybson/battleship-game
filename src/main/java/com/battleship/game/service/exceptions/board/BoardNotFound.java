package com.battleship.game.service.exceptions.board;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class BoardNotFound extends Exception {
    public static final String MESSAGE = "Board not found";

    public BoardNotFound() {
        super(MESSAGE);
    }

}
