package com.battleship.game.service.exceptions.game;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class GameNotFound extends Exception {
    public static final String MESSAGE = "Can not find game";

    public GameNotFound() {
        super(MESSAGE);
    }
}
