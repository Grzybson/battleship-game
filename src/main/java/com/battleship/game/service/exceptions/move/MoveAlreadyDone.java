package com.battleship.game.service.exceptions.move;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class MoveAlreadyDone extends Exception {
    public static final String MESSAGE = "You done your move. opponent turn";

    public MoveAlreadyDone(){
        super(MESSAGE);
    }
}
