package com.battleship.game.service.exceptions.move;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MoveNotFound extends Exception {

    public static final String MESSAGE = "Can not find move";

    public MoveNotFound(){
        super(MESSAGE);
    }
}
