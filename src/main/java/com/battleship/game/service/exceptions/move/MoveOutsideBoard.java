package com.battleship.game.service.exceptions.move;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MoveOutsideBoard extends Exception {
    public static final String MESSAGE = "Can not make move, move outside board";

    public MoveOutsideBoard(){
        super(MESSAGE);
    }
}
