package com.battleship.game.service.exceptions.move;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SecondMoveInTheSameRound extends Exception {

    public static final String MESSAGE = "Can not move in this round again";

    public SecondMoveInTheSameRound(){
        super(MESSAGE);
    }
}
