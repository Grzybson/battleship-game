package com.battleship.game.service.exceptions.ship;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ShipInvalidData extends Exception {

    public static final String MESSAGE = "Invalid ship values in parameter";

    public ShipInvalidData() {
        super(MESSAGE);
    }


}
