package com.battleship.game.service.exceptions.ship;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ShipNotFound extends Exception {

    public static final String MESSAGE = "Do not found this ship";

    public ShipNotFound(){
        super(MESSAGE);
    }
}
