package com.battleship.game.service.exceptions.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictUser extends Exception {

    public static final String MESSAGE = "User with this login already exists";

    public ConflictUser(){
        super(MESSAGE);
    }
}
