package com.battleship.game.service.exceptions.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFound extends Exception {
    public static final String MESSAGE = "Can not find user";

    public UserNotFound(){
        super(MESSAGE);
    }
}
