package com.battleship.game.service.mapper;

import com.battleship.game.dto.board.BoardDto;
import com.battleship.game.dto.board.CreateBoardDto;
import com.battleship.game.model.Board;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class BoardDtoMapper {

    public BoardDto toDto(Board board){
        return new BoardDto(
                board.getId(),
                board.getXSize(),
                board.getYSize());
    }

    public Board toModel(CreateBoardDto createBoardDto) {
        return new Board(UUID.randomUUID().toString(),
                createBoardDto.getXSize(),
                createBoardDto.getYSize());
    }
}
