package com.battleship.game.service.mapper;

import com.battleship.game.dto.game.CreateUpdateGameDto;
import com.battleship.game.dto.game.GameDto;
import com.battleship.game.dto.game.GameFullDto;
import com.battleship.game.model.Game;
import com.battleship.game.model.GameState;
import com.battleship.game.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class GameDtoMapper {

    @Autowired
    private UserService userService;

    public GameDto toDto(Game g) {
        return new GameDto(g.getId(),
                g.getName(),
                g.getRound(),
                g.getPlayer1Id(),
                g.getPlayer2Id(),
                g.getBoard1Id(),
                g.getBoard2Id(),
                g.getStatus());

    }


    public Game toModel(CreateUpdateGameDto gameDto) {
        return Game.builder()
                .id(UUID.randomUUID().toString())
                .name(gameDto.getName())
                .round(gameDto.getRound())
                .player1Id(gameDto.getPlayer1Id())
                .player2Id(gameDto.getPlayer2Id())
                .board1Id(gameDto.getBoard1Id())
                .board2Id(gameDto.getBoard2Id())
                .status(GameState.GAME_NOT_STARTED)
                .build();
    }

    public GameFullDto toFullDto(Game game) {
        return GameFullDto.builder()
                .id(game.getId())
                .board1Id(game.getBoard1Id())
                .board2Id(game.getBoard2Id())
                .player1Id(game.getPlayer1Id())
                .player2Id(game.getPlayer2Id())
                .player1Login(userService.mapIdToLogin(game.getPlayer1Id()))
                .player2Login(userService.mapIdToLogin(game.getPlayer2Id()))
                .name(game.getName())
                .round(game.getRound())
                .status(game.getStatus())
                .build();
    }
}
