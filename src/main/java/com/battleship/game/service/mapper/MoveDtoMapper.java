package com.battleship.game.service.mapper;

import com.battleship.game.dto.move.CreateMoveDto;
import com.battleship.game.dto.move.MoveDto;
import com.battleship.game.model.Move;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class MoveDtoMapper {


    public MoveDto toDto(Move m) {
        return MoveDto.builder()
                .id(m.getId())
                .round(m.getRound())
                .x(m.getX())
                .y(m.getY())
                .boardId(m.getBoardId())
                .build();

    }

    public Move toModel(MoveDto moveDto) {
        return Move.builder()
                .id(UUID.randomUUID()).
                        x(moveDto.getX()).
                        y(moveDto.getY()).
                        boardId(moveDto.getBoardId()).
                        round(moveDto.getRound()).
                        build();
    }

    public Move toModel(CreateMoveDto moveDto) {
        return Move.builder().
                id(UUID.randomUUID()).
                x(moveDto.getX()).
                y(moveDto.getY()).
                boardId(moveDto.getBoardId()).
                round(moveDto.getRound()).
                build();
    }
}
