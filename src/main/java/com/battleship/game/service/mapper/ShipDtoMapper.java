package com.battleship.game.service.mapper;

import com.battleship.game.dto.ship.CreateUpdateShipDto;
import com.battleship.game.dto.ship.ShipDto;
import com.battleship.game.model.Ship;
import com.battleship.game.service.exceptions.ship.ShipInvalidData;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.UUID;

@Component
public class ShipDtoMapper {

    public ShipDto toDto(Ship s) {
        return ShipDto.builder()
                .id(s.getId())
                .boardId(s.getBoardId())
                .x(s.getX())
                .y(s.getY())
                .size(s.getSize())
                .orientation(s.getOrientation())
                .build();

    }

    public Ship toModel(@RequestBody CreateUpdateShipDto shipDto) {
        return Ship.builder()
                .id(UUID.randomUUID().toString())
                .boardId(shipDto.getBoardId())
                .x(shipDto.getX())
                .y(shipDto.getY())
                .size(shipDto.getSize())
                .orientation(shipDto.getOrientation())
                .build();
    }

    public void validate(@RequestBody CreateUpdateShipDto shipDto) throws ShipInvalidData {
        if (shipDto.getBoardId() == null || shipDto.getBoardId().isEmpty()) {
            throw new ShipInvalidData();
        }
    }

}
