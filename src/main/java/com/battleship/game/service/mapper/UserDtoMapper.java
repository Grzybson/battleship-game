package com.battleship.game.service.mapper;

import com.battleship.game.dto.user.CreateUpdateUserDto;
import com.battleship.game.dto.user.UserDto;
import com.battleship.game.model.User;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UserDtoMapper {

    public UserDto toDto(User u) {
        return new UserDto(u.getId(),
                u.getNickname(),
                u.getLogin());
    }

    public User toModel(CreateUpdateUserDto dto) {
        return new User(UUID.randomUUID().toString(),
                dto.getLogin(),
                dto.getPassword(),
                dto.getNickname());
    }

}
